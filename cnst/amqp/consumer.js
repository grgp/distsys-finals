const config = require('../config');
const helpers = require('../../helpers');
const pb = require('./publisher');
const api = require('../api');
const amqp = require('amqplib/callback_api');
const cred = require('amqplib/lib/credentials');
const options = {
  credentials: cred.plain(config.amqp_user, config.amqp_pass)
};

// const str = 'amqp://' + config.amqp_user + ':' + config.amqp_pass + '@' + config.amqp_raw_address + '/' + config.amqp_vhost;

module.exports = {
  consume: (ex, caller) => {
    amqp.connect(config.amqp_address, options, (err, conn) => {
      conn.createChannel((err, ch) => {
        ch.assertQueue('', {exclusive: true}, function(err, q) {
          console.log(' [*] Caller ' + caller + ' waiting for logs on exchange ' + ex);
    
          ch.bindQueue(q.queue, ex, config.request_prefix + caller);
          ch.bindQueue(q.queue, ex, config.response_prefix + caller);
    
          ch.consume(q.queue, (msg) => {
            const obj = JSON.parse(msg.content.toString());
            console.log(' [>] request: ' + JSON.stringify(obj));
            console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString()) 
            if (obj.type === 'request' || true) {
              console.log("It's a request");
              api(obj).then(response => {
                if (response.to_send) {
                  pb.publish(response, obj.sender_id);
                }
              });
            } else if (obj.type === 'response') {
              console.log("It's a response");
              console.log(JSON.stringify(obj));
            }
          }, {noAck: true});
        });
      });
    });
  },
};