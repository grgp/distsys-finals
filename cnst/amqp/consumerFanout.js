const config = require('../config');
const amqp = require('amqplib/callback_api');
const cred = require('amqplib/lib/credentials');
const options = {
  credentials: cred.plain(config.amqp_user, config.amqp_pass),
};

module.exports = () =>
  amqp.connect(config.amqp_address, options, (err, conn) => {
    if (err) {
      console.log('Error in connecting to 172.17.0.3:5672 ' + err);
    }
    
    conn.createChannel((err, ch) => {
      const ex = 'EX_BLUE3';

      ch.assertExchange(ex, 'fanout', {durable: false});
      ch.assertQueue('', {exclusive: true}, (err, q) => {
        // console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', q);
        ch.bindQueue(q.queue, ex, '');

        ch.consume(q.queue, msg => {
          message = msg.content.toString()
          console.log(' [x] %s', message);
          // updatePing(JSON.parse(message)).then(ping_update => {
          //   // console.log('     update quorum by ping: ' + ping_update);
          // });
        }, {noAck: true});
      });
    });
  });
