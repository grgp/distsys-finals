const config = require('../config');
const amqp = require('amqplib/callback_api');
const cred = require('amqplib/lib/credentials');
const options = {
  credentials: cred.plain(config.amqp_user, config.amqp_pass)
};

module.exports = {
  publish: (data, ex, send_from, send_type) => {
    amqp.connect(config.amqp_address, options, (err, conn) => {
      conn.createChannel((err, ch) => {
        const dataString = JSON.stringify(data);
        const routing_key = (data.type === 'request' ? config.request_prefix:config.response_prefix) + send_from;
    
        var durable = true;
        if (send_type == 'fanout') {
          durable = false;
        }

        ch.assertExchange(ex, send_type, {durable: durable});
        ch.publish(ex, routing_key, new Buffer(dataString));
        console.log(" [x] Sent %s: '%s'", routing_key, dataString);
      });
    
      // setTimeout(() => { conn.close(); process.exit(0) }, 500);
    });    
  },
};
