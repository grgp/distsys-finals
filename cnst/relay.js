const config = require('./config');
const cn = require('./amqp/consumer');
const args = process.argv.slice(2);

for (let exchange of Object.values(config.ex_map.relay)) {
  cn.consume(exchange, 'relay');
};
