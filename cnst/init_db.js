#!/usr/bin/env node

const path = require('path');
var sqlite3 = require('sqlite3').verbose();
const root_path = path.join(__dirname);
const args = process.argv.slice(2);
var db_name = 'nu';
if (args.length > 0) {
  db_name = args[0];
  console.log(root_path);
}

const db_path = path.join(root_path + '/db/database-' + db_name + '.db');
var db = new sqlite3.Database(db_path);

db.serialize(function() {
  db.run("\
    CREATE TABLE IF NOT EXISTS messages (\
      id INTEGER PRIMARY KEY AUTOINCREMENT,\
      timestamp TEXT,\
      sender TEXT,\
      message TEXT\
    )\
  ");

  db.run("INSERT OR IGNORE INTO messages VALUES ('1', '1984', 'Itsame', 'Hello, there')");

  console.log("id\t:timestamp\t:sender\t:message");
  db.each("SELECT id, timestamp, sender, message FROM messages", function(err, row) {
    console.log('cnst_messages -- ' + row.id + ": " + row.timestamp + ": " + row.sender + ": " + row.message);
  });
});

db.close();