const path = require('path');
const root_path = path.join(__dirname);
const db_path_prefix = path.join(root_path + '/db/database-');

module.exports = {
  // amqp_address: 'amqp://localhost',
  // amqp_user: 'guest',
  // amqp_pass: 'guest',
  amqp_raw_address: '152.118.148.103',
  amqp_address: 'amqp://152.118.148.103/1406569781',
  amqp_user: '1406569781',
  amqp_pass: '994804',
  amqp_vhost: '1406569781',
  db_path_prefix: db_path_prefix,
  node_names: ['node1', 'node2', 'node3'],
  relay_name: 'relay',
  ex_map: {
    node1: {
      'EX_ORANGE': 'EX_ORANGE',
    },
    node2: {
      'EX_GREEN': 'EX_BLUE',
    },
    node3: {
      'EX_PINK': 'EX_PINK',
    },
    relay: {
      'EX_ORANGE': 'EX_ORANGE',
      'EX_GREEN': 'EX_GREEN',
      'EX_PINK': 'EX_PINK',
    }
  },
  request_prefix: 'REQ_',
  response_prefix: 'RESP_',
};
