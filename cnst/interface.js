#!/usr/bin/env node

const config = require('./config');
const d = new Date();
const pb = require('./amqp/publisher');
const args = process.argv.slice(2);

const action = args[0].toLowerCase();

var data = {
  action: action,
  type: 'request',
  sender: 'relay',
  ts: d.toISOString().replace(/\..*/, '').replace(/T/, ' '),
}

if (data.action == 'relay_write' && args.length >= 3) {
  const send_to = args[1];
  const ex_key = config.ex_map[send_to];
  if (ex_key) {
    const ex = Object.values(ex_key)[0];
    data.message = args[2];
    data.send_to = send_to;
    send_type = 'direct';
    pb.publish(data, ex, data.sender, send_type);
  }
} else if (data.action == 'relay_read') {
  send_type = 'fanout';
  pb.publish(data, 'EX_BLUE3', 'relay', send_type);
} else {
  console.log('Call this interface by this format:');
  console.log('node interface.js relay_write <message>');
  console.log('\nOr:');
  console.log('node interface.js relay_read');
}

setTimeout(() => { process.exit(0) }, 1500);
