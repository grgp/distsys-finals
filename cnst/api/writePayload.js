const path = require('path');
const config = require('../config');
const helpers = require('../../helpers');
const pb = require('../amqp/publisher');

var sqlite3 = require('sqlite3').verbose();

module.exports = (payload) => {
  return new Promise(resolve => {
    const node_name = payload.send_to;
    const db_path = path.join(config.db_path_prefix + node_name + '.db');
    console.log("db path " + db_path);
    var db = new sqlite3.Database(db_path);

    const d = new Date();
    db.run(
      "INSERT OR IGNORE INTO messages VALUES ($id, $timestamp, $sender, $message)", {
        $id: 0,
        $timestamp: helpers.getTimestamp(),
        $sender: payload.sender,
        $message: payload.message
      },
      (err) => {
        var write_payload = 1;
        if (err) { 
          write_payload = -4;
          throw err;
        }
        else {
          var data = {
            action: 'confirm_write_to_relay',
            type: 'response',
            ts: d.toISOString().replace(/\..*/, '').replace(/T/, ' '),
            message: payload.message
          }

          const ex = Object.values(config.ex_map.node_name)[0];
          pb.publish(data, ex, node_name, 'direct');
          resolve({ "write_payload": write_payload });
        }
      }
    );
  });
}



