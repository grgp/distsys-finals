const config = require('../config');

module.exports = (obj) => {
  return new Promise(resolve => {
    console.log("What is obj " + JSON.stringify(obj) );
    if (obj.action == 'relay_write') {
      writePayload(obj).then(response => {
        resolve(response);
      });
    } else if (obj.action == 'confirm_write_to_relay') {
      syncPayloadToOthers(obj).then(response => {
        resolve(response);
      });
    }
  });  
}