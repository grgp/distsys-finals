const config = require('./config');
const cn = require('./amqp/consumer');
const cn_fanout = require('./amqp/consumerFanout');
const args = process.argv.slice(2);

if (args.length > 0) {
  console.log("args[0]: " + args[0]);
  node_name = args[0];
  cn_fanout();
  for (let exchange of Object.values(config.ex_map[node_name])) {
    cn.consume(exchange, args[0]);
  };
}
